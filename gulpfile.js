const gulp = require("gulp");
const uglify = require("gulp-uglify");
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const clean = require("gulp-clean");
const babel = require("gulp-babel");
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const htmlmin = require('gulp-htmlmin');


function handleError (error) {
    console.log(error.toString());
    this.emit('end');
}


// Copy Html files
gulp.task("copyHtml", function(){
    gulp.src("src/*.html")
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest("dist"));
});


// Images
gulp.task("img", function(){
    gulp.src("src/img/**/*")
        .pipe(imagemin({verbose: true}))
        .pipe(gulp.dest("dist/img"));
});


// Compile Sass
gulp.task("sass", function(){
    gulp.src("src/scss/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(cleanCSS({debug: true, compatibility: 'ie7'}, (details) => {
            console.log(`css minify (${details.name}): ${details.stats.originalSize} -> ${details.stats.minifiedSize}`);
        }))
        .pipe(autoprefixer({
            cascade: false,
            grid: true
        }))
        .pipe(gulp.dest("dist/css"));
});


// Scripts
gulp.task("scripts", function(){
    gulp.src(["src/js/0-plugins/*.js", "src/js/1-modules/*.js", "src/js/*.js"])
        .pipe(concat("main.js"))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .on('error', handleError)
        .pipe(uglify({
            compress: {
                drop_console: false,
            },
        }))
        .pipe(gulp.dest("dist/js"));
});


// Clean dist
gulp.task("clean", function(){
    gulp.src('dist', {read: false})
        .pipe(clean());
});


// Make dist
gulp.task("dist", ["copyHtml","img","scripts","sass"]);


// Default
gulp.task("default", ["dist"]);


// Watch
gulp.task("watch", ["dist"], function(){
    gulp.watch("src/*.html", ["copyHtml"]);
    gulp.watch("src/img/**/*", ["img"]);
    gulp.watch("src/scss/**/*.scss", ["sass"]);
    gulp.watch("src/js/**/*.js", ["scripts"]);
});