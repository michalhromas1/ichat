[Version 1.0]

"I Chat With People" blog's code. 

# Setup

1. clone the repository
2. go to repository's root 
3. install [**npm**](https://www.npmjs.com/get-npm) 
    1. if you have it already, run **npm update**
4. run **npm install**
5. run **gulp**
6. **dist** folder should appear
7. open **index.html**
